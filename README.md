# README for hexplot R package #
--------
***hexplot*** is a GSEA-like illustration to subtypeing samples using expression data and gene signatures.

### How to download hexplot? ###
--------
Download the package using git clone or using the "download" link.

     git clone https://bitbucket.org/mthjwu/hexplot

### Software dependencies ###
--------
The package has been tested in R. 
The following R packages need user to install by themselves.

* RColorBrewer
* foreach
* doMC

### input format ###
--------
* exp_matrix: gene expression matrix, with samples in columns and genes in rows.
* gsl: gene list of gene signatures for each subtype, including up and down signature genes (format: gsl = list(sample1=list(up=(), dn=()), sample2=list(up=(), dn=()), sample3=list(up=(), dn=()))). Check the test data for more details.


### How to run hexplot ###
--------
* Load hexplot functions in R

        source("hexplot.R")

* Load test input data

        load("Input_Data.RData")

* calculate signature scores and do permutation test

        sigScore = local.gsl.permutation(exp_matrix, gsl)

* plot hex plot

        pdf("hexplot.pdf", 2.5, 2.5)
		easy.hexplot(sigScore$perm.pval, no.text=F, cex=0.5, lwd=0.2)
		dev.off()

### Who do I talk to? ###
--------
* Hua-Jun Wu (hjwu@jimmy.harvard.edu)
